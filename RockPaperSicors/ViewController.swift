//
//  ViewController.swift
//  RockPaperSicors
//
//  Created by Sanni Prasad on 01/08/17.
//  Copyright © 2017 volansys. All rights reserved.
//

import UIKit
import GameplayKit

class ViewController: UIViewController {
    @IBOutlet var flyoverView: UIView!
    
    
    @IBOutlet weak var RoboBtn: UIButton!
    
    @IBOutlet weak var paperBtn: UIButton!
    @IBOutlet weak var rockBtn: UIButton!
    @IBOutlet weak var scissorsBtn: UIButton!
    
    @IBOutlet weak var playAgainBtn: UIButton!
    
    @IBOutlet weak var winImageFlyover: UIImageView!
    @IBOutlet weak var playAgainbtnFlyover: UIButton!
    
    //MARK: enum of three options
    enum choice{
        case p, r, s
    }
    enum gameStatus{
        case you,cpu,draw
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playAgainBtn.layer.cornerRadius = 3.0
        playAgainBtn.layer.masksToBounds = true
        
        playAgainbtnFlyover.layer.cornerRadius = 3.0
        playAgainbtnFlyover.layer.masksToBounds = true
        flyoverView.layer.cornerRadius = 10.0
        flyoverView.layer.masksToBounds = true
        flyoverView.layer.shadowColor = UIColor.darkGray.cgColor
        flyoverView.layer.shadowRadius = 13.0
        flyoverView.layer.shadowOffset = CGSize(width: 20.0, height: 15.0)
        flyoverView.layer.shadowOpacity = 0.8
        
    }
    
    
    
    //🤖✋👊✂️
    
    
    func CPUchooses() -> Int {
        let cpu = GKRandomDistribution(lowestValue: 0, highestValue: 2).nextInt()
        switch cpu {
        case 0:
            RoboBtn.setTitle("✋", for: .normal)
            
        case 1:
            RoboBtn.setTitle("👊", for: .normal)
            
        case 2:
            RoboBtn.setTitle("✂️", for: .normal)
            
        default:
            RoboBtn.setTitle("🤖", for: .normal)
            
        }
        
        return cpu
    }
    func resetPlay() {
        paperBtn.setTitle("✋", for: .normal)
        rockBtn.setTitle("👊", for: .normal)
        scissorsBtn.setTitle("✂️", for: .normal)
        RoboBtn.setTitle("🤖", for: .normal)
        
        
        
    }
    
    
    func configureFlyover(win:gameStatus) {
        
        flyoverView.center.x = self.view.center.x
        flyoverView.center.y = -100

        
        switch win {
        case .cpu:
            winImageFlyover.image = UIImage(named: "win")
        case .you:
            winImageFlyover.image = UIImage(named: "lose")
        default:
            winImageFlyover.image = UIImage(named: "tie")
        }
        
        
    }
    
    
    func declareWinner(winner:gameStatus) {
        
        configureFlyover(win: winner)
        
        UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseIn, animations: {
            
            self.flyoverView.center = self.view.center
            self.view.addSubview(self.flyoverView)
            
            
        }, completion: nil)
        
        
        
        
        
    }
    
    
    func whoWins(cpu:Int,you: Int)  {
        if cpu == you {
            declareWinner(winner: gameStatus.draw)
        }
        else if cpu == choice.p.hashValue{
            if you == choice.s.hashValue {
                declareWinner(winner: gameStatus.you)
            }
            else{
                declareWinner(winner: gameStatus.cpu)
            }
        }
        else if cpu == choice.r.hashValue{
            if you == choice.p.hashValue {
                declareWinner(winner: gameStatus.you)
            }
            else{
                declareWinner(winner: gameStatus.cpu)
            }
        }
        else if cpu == choice.s.hashValue{
            if you == choice.r.hashValue {
                declareWinner(winner: gameStatus.you)
            }
            else{
                declareWinner(winner: gameStatus.cpu)
            }
        }
        
        
    }
    
    
    
    //MARK: Btn Actions
    
    @IBAction func paperSelected(_ sender: UIButton) {
        
        whoWins(cpu: CPUchooses(), you: choice.p.hashValue)
        
    }
    
    
    @IBAction func rockSelected(_ sender: UIButton) {
        whoWins(cpu: CPUchooses(), you: choice.r.hashValue)
        
    }
    
    @IBAction func ScissorsSelected(_ sender: UIButton) {
        whoWins(cpu: CPUchooses(), you: choice.s.hashValue)
        
    }
    
    //Mark:Play Again
    
    @IBAction func PlayAgainSelected(_ sender: UIButton) {
        resetPlay()
        
    }
    
    @IBAction func PlayAgainFlyover(_ sender: UIButton) {
        resetPlay()
        flyoverView.removeFromSuperview()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print("memory warning recieved")
    }
    
    
}

